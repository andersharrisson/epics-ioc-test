FROM registry.esss.lu.se/ics-docker/miniconda:latest

USER root
RUN yum -y install screen
RUN useradd -ms /bin/bash ioc

USER ioc
RUN conda create \
    --name epics \
    --override-channels \
    --channel https://artifactory.esss.lu.se/artifactory/api/conda/conda-e3-virtual \
    e3-common
RUN echo "conda activate epics" >> ~/.bashrc
COPY --chown=ioc:ioc ioc /home/ioc/
WORKDIR /home/ioc
ENV PREFIX test
CMD /bin/bash -c 'source .bashrc && iocsh.bash st.cmd'
