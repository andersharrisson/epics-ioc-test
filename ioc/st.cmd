# Load standard module startup scripts
iocshLoad("$(E3_COMMON_DIR)/e3-common.iocsh")

cd $(E3_IOCSH_TOP)
dbLoadRecords("db/test.db", "P=$(PREFIX)")

# Call iocInit to start the IOC
iocInit()
